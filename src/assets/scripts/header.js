$(function(){
		$('.header-navOpen').click(function(event) {
			event.preventDefault();
			$('.nav').addClass('active');
			$('.nav_overlay').addClass('active');
			$('body').addClass('active');
			// $('.header_box1').clone().appendTo('.header_nav').insertBefore('.header_nav_enlaces');
		});
		$('.navClose, .nav_overlay').click(function(event) {
			event.preventDefault();
			$('.nav').removeClass('active');
			$('.nav_proyectos_submenu_item').removeClass('active');
			$('.nav_proyectos_background').removeClass('active');
			$('.nav_overlay').removeClass('active');
			$('body').removeClass('active');
			$('.header_proyectos_venta').removeClass('active');
			$('.nav_proyectos_background2').removeClass('active');
		});
		$('.nav-proyectosOpen').click(function(event) {
			event.preventDefault();
			$('.nav_proyectos_background').addClass('active');
			$('.nav_proyectos_submenu_item').addClass('active');
			$('.nav_overlay').addClass('active');
			$('.nav_proyectos_background2').addClass('active');
		});

		$('.navReturn').click(function(event) {
			event.preventDefault();
			$('.nav_proyectos_background2').removeClass('active');
			$('.nav_proyectos_submenu_item').removeClass('active');
		});
		$('.proyectos_ventaOpen').click(function(event) {
			event.preventDefault();
			$('.header_proyectos_venta').addClass('active');
			$('.nav_overlay').addClass('active');
			$('body').addClass('active');
			$('.header_btn__absolute').addClass('active');
		});

		// Ancla scroll - AGREGAR CLASE DEL ENLACE
		$('.header_detalle_nav a, .header_detalle_btn a').click(function() {
			cerrar_nav();
			if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
				var $target = $(this.hash);
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
				if ($target.length) {
				var targetOffset = $target.offset().top-160;
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
				}
			}
		});


		//clone div's desktop to menu resposnsive 
		$('.header-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header-list');
		$('.header-logo').clone().prependTo('.menu-sidebar-cnt').removeClass('header-logo').addClass('responsive-logo');
		$('.header-languages-select').clone().appendTo('.menu-sidebar-cnt');
	
		//events: menu burguer
		function cerrar_nav() {
			$('.nav').removeClass('active');
			$('.nav_proyectos_submenu_item').removeClass('active');
			$('.nav_proyectos_background').removeClass('active');
			$('.nav_overlay').removeClass('active');
			$('body').removeClass('active');
			$('.header_proyectos_venta').removeClass('active');
			$('.nav_proyectos_background2').removeClass('active');
		};
	
		function abrir_nav(){
			$('.nav_proyectos_background').addClass('active');
			$('.nav_proyectos_submenu_item').addClass('active');
			$('.nav_overlay').addClass('active');
			$('.nav_proyectos_background2').addClass('active');
		}
	
		$('.menu-mobile-close , .header-menu-overlay').click(function(event) {
			event.preventDefault();
			cerrar_nav();
		});	
	
		$('.menu-mobile-open').click(function(event) {
			abrir_nav()
		});
	
		//detectando tablet, celular o ipad
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// tasks to do if it is a Mobile Device
			function readDeviceOrientation() {
				if (Math.abs(window.orientation) === 90) {
					// Landscape
					cerrar_nav();
				} else {
					// Portrait
					cerrar_nav();
				}
			}
			window.onorientationchange = readDeviceOrientation;
		}else{
			$(window).resize(function() {
				var estadomenu = $('.menu-responsive').width();
				if(estadomenu != 0){
					cerrar_nav();
				}
			});
		}
		
	
		// header scroll
		var altoScroll = 0
		$(window).scroll(function() {
			altoScroll = $(window).scrollTop();
			if (altoScroll > 0) {
				$('.header-fixed').addClass('scrolling');
			}else{
				$('.header-fixed').removeClass('scrolling');
			};
		});
	});
	