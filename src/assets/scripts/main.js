$(function(){
	$('.form__general__wrapper').each(function(k, el){
		$(el).find('input[type="file"]').change(function(e){
			var id_input = $(this).attr('id');
			$('label[for="'+id_input+'"]').html('').html( e.target.files[0].name);
		});

	});
	$('.btn__work__footer').click(function() {
		event.preventDefault();
		$('.footer__overlay__modal').addClass('active');
		$('.ctn__footer__modal').addClass('active');
		$('body').addClass('active');
	});
	$('.btn__condi').click(function() {
		event.preventDefault();
		$('.footer__overlay__modal').addClass('active');
		$('.ctn__politicas').addClass('active');
		$('body').addClass('active');
	});
	$('.ctn__footer__modal__info__close').click(function() {
		event.preventDefault();
		$('.footer__overlay__modal').removeClass('active');
		$('.ctn__footer__modal').removeClass('active');
		$('body').removeClass('active');
	});
	$('.ctn__politicas__close').click(function() {
		event.preventDefault();
		$('.footer__overlay__modal').removeClass('active');
		$('.ctn__politicas').removeClass('active');
		$('body').removeClass('active');
	});
	$('.footer__overlay__modal').click(function() {
		event.preventDefault();
		$(this).removeClass('active');
		$('.ctn__footer__modal').removeClass('active');
		$('.ctn__politicas').removeClass('active');
		$('body').removeClass('active');
	});
});

